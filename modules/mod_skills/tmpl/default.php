<?php
	/**
	 * @package     Joomla.Site
	 * @subpackage  mod_custom
	 *
	 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
	 * @license     GNU General Public License version 2 or later; see LICENSE.txt
	 */

	defined( '_JEXEC' ) or die;


	$html = $params->get( 'html-skill' );
	$php = $params->get( 'php-skill' );
	$javascript = $params->get( 'javascript-skill' );
	$css = $params->get( 'css-skill' );
	$seo = $params->get( 'seo-skill' );
	$mysql = $params->get( 'mysql-skill' );
?>
<div class="container">
	<div class="row">
	<div class="col-xs-12 ">
		<div class="jumbotron mod_skills <?php echo $moduleclass_sfx ?>">
			<article>
				<?php
					if ( $module->content != '' ) {
						echo $module->content . '<hr>';
					}
				?>
				<div class="row">
					<?php if ( $html != '' || !isset( $html ) ) { ?>
						<div class="col-xs-12 col-sm-4 col-lg-2">
							<span class="min-chart" id="chart-html-skill" data-percent="<?php echo $html; ?>"><span class="percent"></span></span>
							<h5 class="text-center"><span class="label label-default">HTML</span></h5>
							<script>
								$(function () {
									$('.min-chart#chart-html-skill').easyPieChart({
										barColor: "#4caf50",
										onStep  : function (from, to, percent) {
											$(this.el).find('.percent').text(Math.round(percent));
										}
									});
								});
							</script>
						</div>

					<?php } ?>
					<?php if ( $php != '' || !isset( $php ) ) { ?>
						<div class="col-xs-12 col-sm-4 col-lg-2">
							<span class="min-chart" id="chart-php-skill" data-percent="<?php echo $php; ?>"><span class="percent"></span></span>
							<h5 class="text-center"><span class="label label-default">Php</span></h5>
							<script>
								$(function () {
									$('.min-chart#chart-php-skill').easyPieChart({
										barColor: "#4caf50",
										onStep  : function (from, to, percent) {
											$(this.el).find('.percent').text(Math.round(percent));
										}
									});
								});
							</script>
						</div>
					<?php } ?>
					<?php if ( $javascript != '' || !isset( $javascript ) ) { ?>
						<div class="col-xs-12 col-sm-4 col-lg-2">
							<span class="min-chart" id="chart-javascript-skill" data-percent="<?php echo $javascript; ?>"><span class="percent"></span></span>
							<h5 class="text-center"><span class="label label-default">Javascript</span></h5>
							<script>
								$(function () {
									$('.min-chart#chart-javascript-skill').easyPieChart({
										barColor: "#4caf50",
										onStep  : function (from, to, percent) {
											$(this.el).find('.percent').text(Math.round(percent));
										}
									});
								});
							</script>
						</div>
					<?php } ?>
					<?php if ( $css != '' || !isset( $css ) ) { ?>
						<div class="col-xs-12 col-sm-4 col-lg-2">
							<span class="min-chart" id="chart-css-skill" data-percent="<?php echo $css; ?>"><span class="percent"></span></span>
							<h5 class="text-center"><span class="label label-default">Css</span></h5>
							<script>
								$(function () {
									$('.min-chart#chart-css-skill').easyPieChart({
										barColor: "#4caf50",
										onStep  : function (from, to, percent) {
											$(this.el).find('.percent').text(Math.round(percent));
										}
									});
								});
							</script>
						</div>
					<?php } ?>
					<?php if ( $seo != '' || !isset( $seo ) ) { ?>
						<div class="col-xs-12 col-sm-4 col-lg-2">
							<span class="min-chart" id="chart-seo-skill" data-percent="<?php echo $seo; ?>"><span class="percent"></span></span>
							<h5 class="text-center"><span class="label label-default">Seo</span></h5>
							<script>
								$(function () {
									$('.min-chart#chart-seo-skill').easyPieChart({
										barColor: "#4caf50",
										onStep  : function (from, to, percent) {
											$(this.el).find('.percent').text(Math.round(percent));
										}
									});
								});
							</script>
						</div>
					<?php } ?>
					<?php if ( $mysql != '' || !isset( $mysql ) ) { ?>
						<div class="col-xs-12 col-sm-4 col-lg-2">
							<span class="min-chart" id="chart-mysql-skill" data-percent="<?php echo $mysql; ?>"><span class="percent"></span></span>
							<h5 class="text-center"><span class="label label-default">Mysql</span></h5>
							<script>
								$(function () {
									$('.min-chart#chart-mysql-skill').easyPieChart({
											barColor: "#4caf50",
										onStep  : function (from, to, percent) {
											$(this.el).find('.percent').text(Math.round(percent));
										}
									});
								});
							</script>
						</div>
					<?php } ?>
				</div>
			</article>
		</div>
	</div>
	</div>
</div>