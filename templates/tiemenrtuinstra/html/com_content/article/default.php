<?php
	/**
	 * @package     Joomla.Site
	 * @subpackage  Templates.beez3
	 *
	 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
	 * @license     GNU General Public License version 2 or later; see LICENSE.txt
	 */

	defined( '_JEXEC' ) or die;

	$app = JFactory::getApplication();
	$templateparams = $app->getTemplate( true )->params;
	$images = json_decode( $this->item->images );
	$urls = json_decode( $this->item->urls );
	JHtml::addIncludePath( JPATH_COMPONENT . '/helpers' );
	JHtml::_( 'behavior.caption' );

	// Create shortcut to parameters.
	$params = $this->item->params;
	$menu = $app->getMenu();
	$lang = JFactory::getLanguage();
	$urls = json_decode( $this->item->urls );
	if ( $menu->getActive() != $menu->getDefault( $lang->getTag() ) ) {

		?>


		<?php
		if ( isset( $images->image_fulltext ) and !empty( $images->image_fulltext ) ) {
			$image_fulltext = htmlspecialchars( $images->image_fulltext, ENT_COMPAT, 'UTF-8' );
		} else {
			$image_fulltext = 'templates/tiemenrtuinstra/assets/img/background/default_header.jpg';
		}
		?>
		<!--Featured Image-->
		<div id="content-head" class="view overlay hm-white-slight z-depth-2" style="background-image: url(<?php echo $image_fulltext; ?>);">
			<?php
				if ( $urls->urla != '' ) {
					echo '<iframe width="120%" height="470" style="margin-left: -40px;" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="' . $urls->urla . '"></iframe>';
				}
			?>
			<div class="full-bg-img flex-center">
				<ul class="animated fadeIn col-md-12 hidden-md-down">
					<li>


						<img src="templates/tiemenrtuinstra/assets/img/logo/logo1000x1000.png" class="center-block" height="212px">
						<h1 class="h1-responsive flex-item">TiemenRTuinstra.nl</h1>
					</li>
				</ul>
				<a href="#">
					<div class="mask waves-effect waves-light"></div>
				</a>
			</div>
		</div>
	<?php } ?>
<?php
	if (
		( $this->item->text != '' ) &&
		( $this->item->introtext != '' ) ||
		( $this->item->fulltext != '' )
	) { ?>
		<section class="container item-page<?php echo $this->pageclass_sfx ?>">
			<div class="row">
				<div class="col-xs-12">
					<!--Post data-->
					<div class="jumbotron m-1 text-xs-center">
						<h1 class="h1-responsive"><?php echo $this->escape( $this->item->title ); ?></h1>
						<hr>
						<div class="text-justify">
							<?php
								if ( $this->item->fulltext != '' ) {
									echo $this->item->introtext;
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php echo JHtml::_( 'content.prepare', '{loadposition breadcrumbs}' ); ?>

		<article class="container item-page<?php echo $this->pageclass_sfx ?>">
			<div class="row">
				<div class="col-xs-12">
					<div id="article-content" class="jumbotron m-1 text-xs-center">
						<div class="text-justify">
							<?php
								if ( $this->item->fulltext == '' ) {
									echo $this->item->text;
								} else {
									echo $this->item->fulltext;
								}
							?>
						</div>
					</div>
					<!--/.Post data-->
				</div>
			</div>
		</article>
	<?php } ?>