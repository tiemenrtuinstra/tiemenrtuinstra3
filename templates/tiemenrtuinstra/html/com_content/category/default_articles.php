<?php
	/**
	 * @package     Joomla.Site
	 * @subpackage  Templates.beez3
	 *
	 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
	 * @license     GNU General Public License version 2 or later; see LICENSE.txt
	 */

	defined( '_JEXEC' ) or die;

	$app = JFactory::getApplication();

	JHtml::addIncludePath( JPATH_COMPONENT . '/helpers/html' );
	JHtml::_( 'behavior.framework' );

	$params = json_decode($this->category->params);

	$image = $this->category->getParams()->get('image');


	if ( isset( $image ) and !empty( $image ) ) {
	$image_fulltext = htmlspecialchars( $image, ENT_COMPAT, 'UTF-8' );
} else {
	$image_fulltext = 'templates/tiemenrtuinstra/assets/img/default_header.jpg';
} ?>
<!--Featured Image-->
<div id="content-head" class="view overlay hm-white-slight z-depth-2" style="background-image: url(<?php echo $image_fulltext; ?>);">
	<div class="full-bg-img flex-center">
		<ul class="animated fadeIn col-md-12 hidden-md-down">
			<li>
				<img src="templates/tiemenrtuinstra/assets/img/logo/logo1000x1000.png" class="center-block" height="212px">
				<h1 class="h1-responsive flex-item">TiemenRTuinstra.nl</h1>
			</li>
		</ul>
		<a href="#!">
			<div class="mask waves-effect waves-light"></div>
		</a>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<!--Post data-->
			<div class="jumbotron m-1 text-xs-center">
				<h1 class="h1-responsive"><?php echo $params->page_title; ?> <small><?php echo $this->category->title; ?></small></h1>
				<hr>
				<div class="text-justify"><?php echo $this->category->description; ?>
				</div>
			</div>
			<!--/.Post data-->
		</div>
	</div>
</div>
<?php echo JHtml::_('content.prepare', '{loadposition breadcrumbs}'); ?>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<!--Main wrapper-->
			<div id="blog-content" class="jumbotron horizontal-listing z-depth-1 ">

				<?php foreach ( $this->items as $i => &$article ) {
					if ( in_array( $article->access, $this->user->getAuthorisedViewLevels() ) ) { ?>
<!--						--><?php //print_r($article); ?>
						<!--First row-->
						<div class="row blog-list_item">
							<!--Image column-->
							<div class="col-sm-4">
								<div class="view overlay hm-white-slight">
									<?php
										$images = json_decode($article->images);

								 if ( isset( $images->image_fulltext ) and !empty( $images->image_fulltext ) ) {
									$image_fulltext = htmlspecialchars( $images->image_fulltext, ENT_COMPAT, 'UTF-8' );
									}else{
									$image_fulltext = 'templates/tiemenrtuinstra/assets/img/background/default_header.jpg';
									} ?>


									<img src="<?php echo $image_fulltext; ?>" class="img-fluid" alt="">
									<a>
										<div class="mask"></div>
									</a>
								</div>
							</div>
							<!--/.Image column-->

							<!--Content column-->
							<div class="col-sm-8">
								<a href="<?php echo JRoute::_( ContentHelperRoute::getArticleRoute( $article->slug, $article->catid, $article->language ) ); ?>">
									<h2>
										<?php echo $this->escape( $article->title ); ?></h2></a>


								<p><?php
										$in_article = strip_tags( $article->introtext);
										echo strlen($in_article) > 400 ? substr($in_article,0,400)."..." : $in_article;
										 ?></p>
								<a class="btn-floating btn-small btn-default pull-right" href="<?php echo $link; ?>"><i class="fa fa-arrow-right"></i></a>

							</div>
							<!--/.Content column-->

						</div>
						<!--/.First row-->


					<?php
					}
				}
				?>


			</div>
			<!--/.Main wrapper-->
		</div>
	</div>
</div>