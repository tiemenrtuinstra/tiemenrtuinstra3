<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.beez3
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app = JFactory::getApplication();
$templateparams = $app->getTemplate(true)->params;

	$params = json_decode($this->category->params);

	$image = $this->category->getParams()->get('image');
	$tags = new JHelperTags;

	if ( isset( $image ) and !empty( $image ) ) {
		$image_fulltext = htmlspecialchars( $image, ENT_COMPAT, 'UTF-8' );
	} else {
		$image_fulltext = 'templates/tiemenrtuinstra/assets/img/default_header.jpg';
	} ?>
<!--Featured Image-->
<div id="content-head" class="view overlay hm-white-slight z-depth-2" style="background-image: url(<?php echo $image_fulltext; ?>);">
	<div class="full-bg-img flex-center">
		<ul class="animated fadeIn col-md-12 hidden-md-down">
			<li>
				<img src="templates/tiemenrtuinstra/assets/img/logo/logo1000x1000.png" class="center-block" height="212px">
				<h1 class="h1-responsive flex-item">TiemenRTuinstra.nl</h1>
			</li>
		</ul>
		<a href="#!">
			<div class="mask waves-effect waves-light"></div>
		</a>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<!--Post data-->
			<div class="jumbotron m-1 text-xs-center">
				<h1 class="h1-responsive"><?php echo $this->category->title; ?></h1>
				<hr>
				<div class="text-justify"><?php echo $this->category->description; ?>
				</div>
			</div>
			<!--/.Post data-->
		</div>
	</div>
</div>
<?php echo JHtml::_('content.prepare', '{loadposition breadcrumbs}'); ?>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<!--Main wrapper-->
				<div id="blog-content" class="jumbotron horizontal-listing">
					<?php if ( count( $this->children[ $this->category->id ] ) > 0 ) {
						foreach ( $this->children[ $this->category->id ] as $id => $child ) {
							if ( $this->params->get( 'show_empty_categories' ) || $child->numitems || count( $child->getChildren() ) ) {
								$params = json_decode($child->params);
								$link = JRoute::_( ContentHelperRoute::getCategoryRoute( $child->id ) );
								?>

								<div class="row blog-list_item">
									<!--Image column-->
									<div class="col-sm-4">
										<div class="view overlay hm-white-slight">
											<img src="<?php echo htmlspecialchars( $params->image, ENT_COMPAT, 'UTF-8' ); ?>" class="img-fluid" alt="<?php echo $image->image_alt; ?>">
											<a href="<?php echo $link; ?>">
												<div class="mask"></div>
											</a>
										</div>
									</div>
									<!--/.Image column-->

									<!--Content column-->
									<div class="col-sm-8">
										<a href="<?php echo $link; ?>"><h2><?php echo $this->escape( $child->title ); ?></h2></a>


										<ul class="label_list">
											<?php
												$tags->getItemTags( 'com_content.category', $child->id );

												foreach ( $tags->itemTags as $itemTag ) {
													echo '<li><span class="label label-default">' . $itemTag->title . '</span></li>';
												}


											?>
										</ul>

											<?php
												$in_article = strip_tags( JHtml::_( 'content.prepare', $child->description, '', 'com_content.category'  ));
												echo strlen( $in_article ) > 250 ? substr( $in_article, 0, 250 ) . "..." : $in_article;
											?>
																			</p>
										<a class="btn-floating btn-small btn-default pull-right" href="<?php echo $link; ?>"><i class="fa fa-arrow-right"></i></a>
									</div>
									<!--/.Content column-->
								</div>
							<?php }
						}
					} ?>
				</div>
			<!--/.Main wrapper-->
		</div>
	</div>
</div>


