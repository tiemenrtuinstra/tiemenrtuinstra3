<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.beez3
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app = JFactory::getApplication();
$templateparams = $app->getTemplate(true)->params;


JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
JHtml::_('behavior.caption');

$pageClass = $this->params->get('pageclass_sfx');

	if(isset($this->children) || !empty( $this->children)){
		echo $this->loadTemplate('children');
	}else{
		echo $this->loadTemplate('articles');
	}
 ?>
