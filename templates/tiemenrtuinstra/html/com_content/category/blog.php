<?php
	/**
	 * @package     Joomla.Site
	 * @subpackage  Templates.beez3
	 *
	 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
	 * @license     GNU General Public License version 2 or later; see LICENSE.txt
	 */

	defined( '_JEXEC' ) or die;

	$app = JFactory::getApplication();
	$templateparams = $app->getTemplate( true )->params;

	JHtml::addIncludePath( JPATH_COMPONENT . '/helpers' );
	JHtml::_( 'behavior.caption' );

	$params = json_decode( $this->category->params );
	$tags = new JHelperTags;

			$image = $this->category->getParams()->get( 'image' );


	if ( isset( $image ) and !empty( $image ) ) {
		$image_fulltext = htmlspecialchars( $image, ENT_COMPAT, 'UTF-8' );
	} else {
		$image_fulltext = 'templates/tiemenrtuinstra/assets/img/default_header.jpg';
	} ?>
<!--Featured Image-->
<div id="content-head" class="view overlay hm-white-slight z-depth-2" style="background-image: url(<?php echo $image_fulltext; ?>);">
	<div class="full-bg-img flex-center">
		<ul class="animated fadeIn col-md-12 hidden-md-down">
			<li>
				<img src="templates/tiemenrtuinstra/assets/img/logo/logo1000x1000.png" class="center-block" height="212px">
				<h1 class="h1-responsive flex-item">TiemenRTuinstra.nl</h1>
			</li>
		</ul>
		<a href="#!">
			<div class="mask waves-effect waves-light"></div>
		</a>
	</div>
</div>
<section class="container">
	<div class="row">
		<div class="col-xs-12">
			<!--Post data-->
			<div class="jumbotron m-1 text-xs-center">
				<h1 class="h1-responsive"><?php echo $params->page_title; ?>
					<small><?php echo $this->category->title; ?></small>
				</h1>
				<hr>
				<div class="text-justify"><?php echo $this->category->description; ?>
				</div>
			</div>
			<!--/.Post data-->
		</div>
	</div>
</section>

<?php echo JHtml::_('content.prepare', '{loadposition breadcrumbs}'); ?>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<!--Main wrapper-->
			<div id="blog-content" class="jumbotron horizontal-listing z-depth-1 ">
<?php
	echo $this->loadTemplate('children');

	echo $this->loadTemplate('item');

?>


			</div>
			<!--/.Main wrapper-->
		</div>
	</div>
</div>