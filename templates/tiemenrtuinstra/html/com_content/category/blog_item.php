<?php
	/**
	 * @package     Joomla.Site
	 * @subpackage  Templates.beez3
	 *
	 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
	 * @license     GNU General Public License version 2 or later; see LICENSE.txt
	 */

	defined( '_JEXEC' ) or die;

	$app = JFactory::getApplication();
	$templateparams = $app->getTemplate( true )->params;

	JHtml::addIncludePath( JPATH_COMPONENT . '/helpers' );
	JHtml::_( 'behavior.caption' );

	$cparams = JComponentHelper::getParams( 'com_media' );
	$tags = new JHelperTags;
	// If the page class is defined, add to class as suffix.
	// It will be a separate class if the user starts it with a space

	$page_title = '';
	$page_content = '';
	if ( empty( $this->lead_items ) && empty( $this->link_items ) && empty( $this->intro_items ) ) {
		if ( $this->params->get( 'show_no_articles', 1 ) ) {
			$page_title = '<h1 class="h1-responsive">' . $this->escape( $this->params->get( 'page_heading' ) ) . '</h1>';
			$page_content = $this->loadTemplate( 'children' );
		}
	}
?>


<?php foreach ( $this->items as $i => &$article ) {
	if ( in_array( $article->access, $this->user->getAuthorisedViewLevels() ) ) { ?>
		<?php $link = JRoute::_( ContentHelperRoute::getArticleRoute( $article->slug, $article->catid, $article->language ) ); ?>
		<!--First row-->
		<div class="row blog-list_item">
			<!--Image column-->
			<div class="col-sm-4">
				<div class="view overlay hm-white-slight">
					<?php
						$images = json_decode( $article->images );

						if ( isset( $images->image_fulltext ) and !empty( $images->image_fulltext ) ) {
							$image_fulltext = htmlspecialchars( $images->image_fulltext, ENT_COMPAT, 'UTF-8' );
						} else {
							$image_fulltext = 'templates/tiemenrtuinstra/assets/img/background/default_header.jpg';
						} ?>


					<img src="<?php echo $image_fulltext; ?>" class="img-fluid" alt="">
					<a href="<?php echo $link; ?>">
						<div class="mask"></div>
					</a>
				</div>
			</div>
			<!--/.Image column-->

			<!--Content column-->
			<div class="col-sm-8">
				<a href="<?php echo $link; ?>">
					<h2>
						<?php echo $this->escape( $article->title ); ?>


					</h2></a>

				<ul class="label_list">
					<?php
						$tags->getItemTags( 'com_content.article', $article->id );

						foreach ( $tags->itemTags as $itemTag ) {
							echo '<li><span class="label label-default">' . $itemTag->title . '</span></li>';
						}


					?>
				</ul>
				<p><?php
						$in_article = strip_tags( $article->introtext );
						echo strlen( $in_article ) > 250 ? substr( $in_article, 0, 250 ) . "..." : $in_article;
					?></p>
				<a class="btn-floating btn-small btn-default pull-right" href="<?php echo $link; ?>"><i class="fa fa-arrow-right"></i></a>

			</div>
			<!--/.Content column-->

		</div>
		<!--/.First row-->


		<?php
	}
}
?>