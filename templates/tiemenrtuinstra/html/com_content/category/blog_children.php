<?php
	/**
	 * @package     Joomla.Site
	 * @subpackage  Templates.beez3
	 *
	 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
	 * @license     GNU General Public License version 2 or later; see LICENSE.txt
	 */

	defined( '_JEXEC' ) or die;

	$app = JFactory::getApplication();
	$templateparams = $app->getTemplate( true )->params;
	$tags = new JHelperTags;
?>

<?php if ( count( $this->children[ $this->category->id ] ) > 0 ) {
	foreach ( $this->children[ $this->category->id ] as $id => $child ) {
		if ( $this->params->get( 'show_empty_categories' ) || $child->numitems || count( $child->getChildren() ) ) {
			$images = json_decode($child->params);
			if ( isset( $images->image ) and !empty( $images->image ) ) {
				$image_fulltext = htmlspecialchars( $images->image, ENT_COMPAT, 'UTF-8' );
			} else {
				$image_fulltext = 'templates/tiemenrtuinstra/assets/img/background/default_header.jpg';
			}
			?>

			<div class="row blog-list_item">
				<!--Image column-->
				<div class="col-sm-4">
					<div class="view overlay hm-white-slight">
						<img src="<?php echo htmlspecialchars( $image_fulltext, ENT_COMPAT, 'UTF-8' ); ?>" class="img-fluid" alt="<?php echo $image->image_alt; ?>">
						<a href="<?php echo JRoute::_( ContentHelperRoute::getCategoryRoute( $child->id ) ); ?>">
							<div class="mask"></div>
						</a>
					</div>
				</div>
				<!--/.Image column-->

				<!--Content column-->
				<div class="col-sm-8">
					<a href="<?php echo JRoute::_( ContentHelperRoute::getCategoryRoute( $child->id ) ); ?>"><h2><?php echo $this->escape( $child->title ); ?></h2></a>
					<ul class="label_list">
						<?php
							$tags->getItemTags( 'com_content.category', $child->id );

							foreach ( $tags->itemTags as $itemTag ) {
								echo '<li><span class="label label-default">' . $itemTag->title . '</span></li>';
							}


						?>
					</ul>
					<?php
						$in_article = strip_tags( JHtml::_( 'content.prepare', $child->description, '', 'com_content.category'  ));
						echo strlen( $in_article ) > 250 ? substr( $in_article, 0, 250 ) . "..." : $in_article;
					?>
					<a class="btn-floating btn-small btn-default pull-right" href="<?php echo $link; ?>"><i class="fa fa-arrow-right"></i></a>
				</div>
				<!--/.Content column-->
			</div>
		<?php }
	}
} ?>

