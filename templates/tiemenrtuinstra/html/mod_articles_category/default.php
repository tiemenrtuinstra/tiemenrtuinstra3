<?php
	/**
	 * @package     Joomla.Site
	 * @subpackage  mod_articles_category
	 *
	 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
	 * @license     GNU General Public License version 2 or later; see LICENSE.txt
	 */

	defined( '_JEXEC' ) or die;

?>
<div id="content" class="container category-module<?php echo $moduleclass_sfx; ?>">
	<div class="row">
		<?php foreach ( $list as $item ){
//			print_r($item->images);
			$images = json_decode( $item->images);

			if ( isset( $images->image_fulltext ) and !empty( $images->image_fulltext ) ) {
				$image_fulltext = htmlspecialchars( $images->image_fulltext, ENT_COMPAT, 'UTF-8' );
			} else {
				$image_fulltext = 'templates/tiemenrtuinstra/assets/img/background/default_header.jpg';
			}
			?>
			<div class="col-xs-12 col-md-4">
				<!--Card-->
				<div class="card">

					<!--Card image-->
					<div class="view overlay hm-white-slight">
						<img src="<?php echo $image_fulltext; ?>" class="img-fluid" alt="">
						<a href="<?php echo $item->link; ?>">
							<div class="mask waves-effect waves-light"></div>
						</a>
					</div>
					<!--/.Card image-->

					<!--Card content-->
					<div class="card-block">
						<!--Title-->
						<h4 class="card-title"><?php echo $item->title; ?></h4>
						<!--Text-->
						<p class="card-text"><?php
								$in_article = strip_tags( $item->introtext );
								echo strlen( $in_article ) > 100 ? substr( $in_article, 0, 100 ) . "..." : $in_article;
							?></p>
						<a class="btn-floating btn-small btn-default pull-right" href="<?php echo $item->link; ?>"><i class="fa fa-arrow-right"></i></a>
					</div>
					<!--/.Card content-->

				</div>
				<!--/.Card-->
			</div>
		<?php } ?>
	</div>
</div>
