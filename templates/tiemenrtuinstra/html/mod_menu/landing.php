<?php
	/**
	 * @package     Joomla.Site
	 * @subpackage  mod_menu
	 *
	 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
	 * @license     GNU General Public License version 2 or later; see LICENSE.txt
	 */

	defined( '_JEXEC' ) or die;

	$db =& JFactory::getDBO();
	$id = 'landing_carrousel';
?>


<div id="<?php echo $id; ?>" class="carousel slide carousel-fade z-depth-2" data-ride="carousel" data-interval="false">
	<!--Indicators-->
	<ol class="carousel-indicators">
		<?php
			$count = 0;
			foreach ( $list as $i => &$item ) {
				if ( $item->home == 1 ) {
					unset( $list[ $i ] );
				}
			}
			$count_items = count( $list );

			while ( $count < $count_items ) {
				$class = '';
				if ( $count == 0 ) {
					$class = 'active';
				}
				echo '<li data-target="#' . $id . '" data-slide-to="' . $count . '" class="' . $class . '"></li>';
				$count++;
			}
		?>
	</ol>

	<!--/.Indicators-->

	<!--Slides-->
	<div class="carousel-inner" role="listbox">
		<?php
			$count = 0;

			foreach ( $list as $i => &$item ) {
				$class = '';
				if ( $count == 0 ) {
					$class = 'active';
				}
				$count++;
				$item_id = $item->query[ 'id' ];

				$item_type = $item->query[ 'view' ];
				$image = 'templates/tiemenrtuinstra/assets/img/background/default_header.jpg';
				if ( $item->menu_image != '' ) {
					$image = $item->menu_image;
				} elseif ( $item_type == 'article' ) {
					//get article image
					$query = "SELECT `images` FROM #__content WHERE id = " . intval( $item_id );
					$db->setQuery( $query );
					$res = $db->loadRow();
					$images = json_decode( $res[ 0 ] );
					$article_image = $images->image_fulltext;

					if( (!isset($article_image))||($article_image == '')){
						$image = 'templates/tiemenrtuinstra/assets/img/background/default_header.jpg';
					}else{
						$image = $article_image;
					}
				} elseif ( $item_type == 'category' ) {
					//get cat image
					$query = "SELECT params FROM jmtrt_categories WHERE id = " . intval( $item_id );
					$db->setQuery( $query );
					$res = $db->loadRow();

					$result = str_replace( '"category_layout"', '', $res[ 0 ] );
					$result = str_replace( '"image"', '', $result );
					$result = str_replace( '"image_alt"', '', $result );
					$result = str_replace( '"', '', $result );
					$result = str_replace( '{', '', $result );
					$result = str_replace( '}', '', $result );
					$result = str_replace( ':', '', $result );
					$cat_image = str_replace( ',', '', $result );

					if( (!isset($cat_image))||($cat_image == '') ){
						$image = 'templates/tiemenrtuinstra/assets/img/background/default_header.jpg';
					}else{
						$image = $cat_image;
					}
				} else {
					$image = 'templates/tiemenrtuinstra/assets/img/background/default_header.jpg';
				}

				?>
				<div class="carousel-item <?php echo $class; ?>" style="background-image:url(<?php echo htmlspecialchars( $image, ENT_COMPAT, 'UTF-8' ); ?>)">
					<div class="view overlay hm-white-slight">
						<div class="full-bg-img flex-center">
							<ul class="animated fadeIn col-md-12">
								<li>
									<img src="templates/tiemenrtuinstra/assets/img/logo/logo1000x1000.png" class="center-block" height="212px">
									<h1 class="h1-responsive flex-item">TiemenRTuinstra.nl</h1>
								</li>
								<li>
									<h2 class="flex-item"><?php echo $item->title; ?></h2>
								</li>
								<li>
									<a href="<?php echo htmlspecialchars( $item->flink ); ?>" class="btn btn-default btn-xs flex-item waves-effect waves-light">Learn
										more</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			<?php } ?>

	</div>
	<!--/.Slides-->

	<!--Controls-->
	<a class="btn-floating btn-default waves-effect waves-light left carousel-control" href="#<?php echo $id; ?>" role="button" data-slide="prev">
		<i class="fa fa-chevron-left" aria-hidden="true"></i>
	</a>
	<a class="btn-floating btn-default waves-effect waves-light  right carousel-control" href="#<?php echo $id; ?>" role="button" data-slide="next">
		<i class="fa fa-chevron-right" aria-hidden="true"></i>
	</a>
	<!--/.Controls-->
</div>

<script>
	$(document).ready(function () {
		$('#<?php echo $id; ?>').carousel();
		$('#<?php echo $id; ?>').hammer().on('swipeleft', function () {
			$(this).carousel('next');
		})
		$('#<?php echo $id; ?>').hammer().on('swiperight', function () {
			$(this).carousel('prev');
		})
	});
</script>
