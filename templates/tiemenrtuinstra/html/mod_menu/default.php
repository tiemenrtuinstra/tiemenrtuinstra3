<?php
	/**
	 * @package     Joomla.Site
	 * @subpackage  mod_menu
	 *
	 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
	 * @license     GNU General Public License version 2 or later; see LICENSE.txt
	 */

	defined( '_JEXEC' ) or die;

	$id = '';

	if ( ( $tagId = $params->get( 'tag_id', '' ) ) ) {
		$id = ' id="' . $tagId . '"';
	}

	//	print_r($list);
	// The menu class is deprecated. Use nav instead
?>


<!--Navbar-->
<nav class="navbar navbar-dark navbar-fixed-top scrolling-navbar">
	<!--Navbar Brand-->
	<a class="navbar-brand hidden-lg-up" href="http://tiemenrtuinstra.nl"><img src="templates/tiemenrtuinstra/assets/img/logo/logo-navbar.png">TiemenRTuinstra.nl</a>
	<!-- Collapse button-->
	<!-- SideNav slide-out button -->

	<a href="#" data-activates="slide-out" class="button-collapse waves-effect waves-light"><i class="fa fa-bars"></i></a>
	<!--/. SideNav slide-out button -->

	<div class="container">

		<!--Collapse content-->
		<div class="hidden-md-down">


			<ul class="nav navbar-nav menu<?php echo $class_sfx; ?>"<?php echo $id; ?>>
				<?php
					foreach ( $list as $i => &$item ) {
						if ( $item->home == 1 ) {
							unset( $list[ $i ] );
						}
					}
					foreach ( $list as $i => &$item ) {

						$class = 'item-' . $item->id;

						if ( $item->id == $default_id ) {
							$class .= ' default';
						}

						if ( ( $item->id == $active_id ) || ( $item->type == 'alias' && $item->params->get( 'aliasoptions' ) == $active_id ) ) {
							$class .= ' current';
						}

						if ( in_array( $item->id, $path ) ) {
							$class .= ' active';
						} elseif ( $item->type == 'alias' ) {
							$aliasToId = $item->params->get( 'aliasoptions' );

							if ( count( $path ) > 0 && $aliasToId == $path[ count( $path ) - 1 ] ) {
								$class .= ' active';
							} elseif ( in_array( $aliasToId, $path ) ) {
								$class .= ' alias-parent-active';
							}
						}

						if ( $item->type == 'separator' ) {
							$class .= ' divider';
						}

						if ( $item->deeper ) {
							$class .= ' deeper';
						}

						if ( $item->parent ) {
							$class .= ' parent';
						}

						echo '<li class="nav-item ' . $class . '">';

						$attributes = array();

						if ( $item->anchor_title ) {
							$attributes[ 'title' ] = $item->anchor_title;
						}

						$attributes[ 'class' ] = 'nav-link';
						if ( $item->anchor_css ) {
							$attributes[ 'class' ] .= $item->anchor_css;
						}

						if ( $item->anchor_rel ) {
							$attributes[ 'rel' ] = $item->anchor_rel;
						}

						$linktype = $item->title;


						if ( $item->browserNav == 1 ) {
							$attributes[ 'target' ] = '_blank';
						} elseif ( $item->browserNav == 2 ) {
							$options = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes';

							$attributes[ 'onclick' ] = "window.open(this.href, 'targetWindow', '" . $options . "'); return false;";
						}

						echo JHtml::_( 'link', JFilterOutput::ampReplace( htmlspecialchars( $item->flink ) ), $linktype, $attributes );


						// The next item is deeper.
						if ( $item->deeper ) {
							echo '<ul class="nav-child unstyled small">';
						} // The next item is shallower.
						elseif ( $item->shallower ) {
							echo '</li>';
							echo str_repeat( '</ul></li>', $item->level_diff );
						} // The next item is on the same level.
						else {
							echo '</li>';
						}
					}
				?></ul>

		</div>
		<!--/.Collapse content-->

	</div>

</nav>


<!-- Sidebar navigation -->
<ul id="slide-out" class="side-nav fixed default-side-nav light-side-nav">

	<!-- Logo -->
	<div class="logo-wrapper waves-light z-depth-1">

	</div>

	<!--/. Logo -->

	<!-- Side navigation links -->
	<ul class="collapsible collapsible-accordion">

		<?php foreach ( $list as $i => &$item ) {
			$class = 'item-' . $item->id;

			if ( $item->id == $default_id ) {
				$class .= ' default';
			}

			if ( ( $item->id == $active_id ) || ( $item->type == 'alias' && $item->params->get( 'aliasoptions' ) == $active_id ) ) {
				$class .= ' current';
			}

			if ( in_array( $item->id, $path ) ) {
				$class .= ' active';
			} elseif ( $item->type == 'alias' ) {
				$aliasToId = $item->params->get( 'aliasoptions' );

				if ( count( $path ) > 0 && $aliasToId == $path[ count( $path ) - 1 ] ) {
					$class .= ' active';
				} elseif ( in_array( $aliasToId, $path ) ) {
					$class .= ' alias-parent-active';
				}
			}

			if ( $item->type == 'separator' ) {
				$class .= ' divider';
			}

			if ( $item->deeper ) {
				$class .= ' deeper';
			}

			if ( $item->parent ) {
				$class .= ' parent';
			}

			echo '<li class="' . $class . '">';

			$attributes = array();

			if ( $item->anchor_title ) {
				$attributes[ 'title' ] = $item->anchor_title;
			}

			$attributes[ 'class' ] = 'collapsible-header waves-effect';
			if ( $item->anchor_css ) {
				$attributes[ 'class' ] .= $item->anchor_css;
			}

			if ( $item->anchor_rel ) {
				$attributes[ 'rel' ] = $item->anchor_rel;
			}

			$linktype = $item->title;

			if ( $item->menu_image ) {
				$linktype = JHtml::_( 'image', $item->menu_image, $item->title );

				if ( $item->params->get( 'menu_text', 1 ) ) {
					$linktype .= '<span class="image-title">' . $item->title . '</span>';
				}
			}

			if ( $item->browserNav == 1 ) {
				$attributes[ 'target' ] = '_blank';
			} elseif ( $item->browserNav == 2 ) {
				$options = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes';

				$attributes[ 'onclick' ] = "window.open(this.href, 'targetWindow', '" . $options . "'); return false;";
			}

			echo JHtml::_( 'link', JFilterOutput::ampReplace( htmlspecialchars( $item->flink ) ), $linktype, $attributes );


			// The next item is deeper.
			if ( $item->deeper ) {
				echo '<div class="collapsible-body"><ul class="nav-child unstyled small">';
			} // The next item is shallower.
			elseif ( $item->shallower ) {
				echo '</li>';
				echo str_repeat( '</ul></div></li>', $item->level_diff );
			} // The next item is on the same level.
			else {
				echo '</li>';
			}
		}
		?>
	</ul>
	<!--/. Side navigation links -->
</ul>
