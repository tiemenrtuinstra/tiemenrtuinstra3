<?php include_once ('header.php'); ?>

	<!--Featured Image-->
	<div id="content-head" class="view z-depth-2">
		<?php $map_url = 'http://www.openstreetmap.org/export/embed.html?bbox=5.0592041015625%2C52.1086138141688%2C5.51239013671875%2C52.300081389496114&layer=mapnik'; ?>
		<iframe width="120%" height="470" style="margin-left: -40px;" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php echo $map_url; ?>"></iframe>
		<div class="full-bg-img flex-center">
			<ul class="animated fadeIn col-md-12">
				<li>
					<img src="template/tiemenrtuinstra/assets/img/logo/logo1000x1000.png" class="center-block" height="212px">
					<h1 class="h1-responsive flex-item">TiemenRTuinstra.nl</h1>
				</li>
			</ul>
			<a href="#!">
				<div class="mask waves-effect waves-light"></div>
			</a>
		</div>
	</div>
	<div class="container"><div class="row"><div class="col-xs-12">
				<!--Post data-->
				<div class="jumbotron m-1 text-xs-center">
					<h1 class="h1-responsive">Contact</h1>
					<hr>
					<div class="text-justify">

						<form>
							<div class="row">

								<!--First column-->
								<div class="col-md-4">
									<div class="md-form">
										<i class="fa fa-user prefix"></i>
										<input type="text" id="form41" class="form-control">
										<label for="form41" class="">Voornaam</label>
									</div>
								</div>

								<!--Second column-->
								<div class="col-md-4">
									<div class="md-form">
										<input type="text" id="form51" class="form-control">
										<label for="form51" class="">Achternaam</label>
									</div>
								</div>


							</div>
							<!--First row-->
							<div class="row">
								<!--First column-->
								<div class="col-md-6">
									<div class="md-form">
										<i class="fa fa-envelope prefix"></i>
										<input type="email" id="form81" class="form-control validate">
										<label for="form81" data-error="wrong" data-success="right">Type your email</label>
									</div>
								</div>

							</div>
							<div class="row">
								<!--First column-->
								<div class="col-md-6">
									<div class="md-form">
										<i class="fa fa-phone prefix"></i>
										<input type="tel" id="form81" class="form-control validate">
										<label for="form81" data-error="wrong" data-success="right">Telefoon</label>
									</div>
								</div>

							</div>
							<!--/.First row-->

							<!--Second row-->
							<div class="row">
								<!--First column-->
								<div class="col-md-12">

									<div class="md-form">
										<i class="fa fa-pencil prefix"></i>
										<textarea type="text" id="form76" class="md-textarea" style="height: 100px;"></textarea>
										<label for="form76">Bericht</label>
									</div>

								</div>
							</div>
							<!--/.Second row-->

							<!--Third row-->
							<div class="row">
								<!--First column-->
								<div class="col-md-12">

									<div class="md-form">
										<button type="submit" class="btn btn-default flex-item waves-effect waves-light pull-right">Verstuur</button>
										<button type="reset" class="btn btn-default-outline flex-item waves-effect waves-light pull-right">Leegmaken</button>
									</div>

								</div>
							</div>
							<!--/.Third row-->
						</form>
					</div>
				</div>
				<!--/.Post data-->
			</div></div></div>


<?php include_once ('footer.php'); ?>