<?php include_once ('header.php'); ?>

    <!--Featured Image-->
    <div id="content-head" class="view overlay hm-white-slight z-depth-2" style="background-image: url(assets/img/background/background2.jpg);">
        <div class="full-bg-img flex-center">
            <ul class="animated fadeIn col-md-12">
                <li>
                    <img src="template/tiemenrtuinstra/assets/img/logo/logo1000x1000.png" class="center-block" height="212px">
                    <h1 class="h1-responsive flex-item">TiemenRTuinstra.nl</h1>
                </li>
            </ul>
            <a href="#!">
                <div class="mask waves-effect waves-light"></div>
            </a>
        </div>
    </div>
<article class="container"><div class="row"><div class="col-xs-12">
    <!--Post data-->
    <div class="jumbotron m-1 text-xs-center">
        <h1 class="h1-responsive">Post title</h1>
        <hr>
     <div class="text-justify">
         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id volutpat nibh. Duis rutrum non lectus at fringilla. Fusce tincidunt rutrum lacus sed iaculis. Proin hendrerit arcu sed justo tempus gravida. Donec pharetra nibh sit amet lacus porttitor, ut tempor ante tincidunt. Pellentesque a pretium nulla, eleifend volutpat est. Integer ultricies diam ipsum, vel fermentum sem vestibulum id.
         <br/><br/>
         <img src="assets/img/background/background2.jpg" class="img-fluid pull-md-right" width="400px" height="auto">
         Sed laoreet, tortor ac rhoncus bibendum, nisi quam congue dolor, ut volutpat est neque in leo. Etiam pretium aliquet nunc quis mollis. Duis tincidunt tincidunt mi in viverra. Aliquam velit velit, semper et luctus sed, eleifend ut nisl. Vivamus commodo ultricies dui et aliquet. Integer diam turpis, luctus nec erat eget, suscipit gravida augue. Praesent pharetra lacus eu quam dapibus, ut rhoncus felis efficitur. Duis consequat magna nec enim euismod, at rutrum felis dapibus. Vestibulum a gravida mauris. Quisque porttitor ac eros ac egestas. Sed tincidunt lectus purus, a congue dolor imperdiet quis. Vivamus nunc mi, condimentum eget vulputate in, maximus et ligula. In lobortis varius nibh ut ultricies. Curabitur nisl ligula, aliquet vitae cursus sed, vestibulum vel mauris. Aliquam tristique est at diam dignissim, non auctor libero semper.
         <br/><br/>
         Integer pellentesque, mauris sed consequat vulputate, diam dui lacinia odio, at faucibus odio tellus eget diam. Integer feugiat sapien in nisl placerat ultricies. Nulla vel imperdiet lorem, sed blandit felis. Nam ornare elit dui, ac feugiat elit luctus sit amet. Phasellus placerat id lacus sed luctus. Pellentesque rhoncus at tellus in lobortis. Vivamus felis dui, iaculis ac purus eu, dignissim accumsan felis. Duis quis porta nisi, non tincidunt justo. Duis sodales scelerisque tristique. Etiam hendrerit pretium nisi eu placerat. Pellentesque convallis dolor et odio tristique sagittis. Integer rutrum ipsum et euismod placerat.
         <br/><br/>
         Vestibulum et est eget arcu pellentesque vestibulum. Quisque ullamcorper fringilla nibh, at lacinia arcu vestibulum in. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nullam eget feugiat sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nunc facilisis nibh vel massa imperdiet condimentum. Maecenas quis justo orci. Donec ut tortor et leo posuere egestas et vel lorem. Pellentesque volutpat consequat nisi, id aliquam arcu laoreet vel. Donec nec magna eget nulla auctor tincidunt ut vel est. Aliquam tempor augue vitae nunc scelerisque, eget lacinia tellus convallis. Donec quis luctus magna, in finibus orci. Morbi vulputate tortor risus, in aliquam erat cursus in. Aliquam malesuada bibendum nisl quis rhoncus. Duis mollis lacus felis, egestas blandit dolor interdum ut. Mauris sollicitudin finibus odio, tempus imperdiet felis dictum ac.
         <br/><br/>
         Integer massa diam, semper sit amet sodales at, molestie ut diam. Proin convallis vehicula diam eget malesuada. Pellentesque vitae accumsan arcu. Vestibulum sollicitudin, eros eu tempor accumsan, odio lorem sollicitudin sapien, vel facilisis tortor neque ac augue. Proin egestas condimentum erat sit amet vehicula. Etiam fringilla laoreet velit, ac feugiat augue elementum sit amet. Integer aliquet hendrerit ligula, sed rhoncus dolor ultricies sed. Proin ac urna tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed aliquet nisi orci, et tristique metus tincidunt eu.</p>

     </div>
    </div>
    <!--/.Post data-->
    </div></div></article>


<?php include_once ('footer.php'); ?>