<?php
	/**
	 * @package     Joomla.Site
	 * @subpackage  Templates.beez3
	 *
	 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
	 * @license     GNU General Public License version 2 or later; see LICENSE.txt
	 */

	// No direct access.
	defined( '_JEXEC' ) or die;
	JHtml::_( 'behavior.framework', true );
	JLoader::import( 'joomla.filesystem.file' );

	$config = JFactory::getConfig();
	$app = JFactory::getApplication();
	$jinput = JFactory::getApplication()->input;

	$doc = JFactory::getDocument();
	$menu = $app->getMenu();
	$lang = JFactory::getLanguage();
	require_once 'classes/Template.php';
	$template = new Template();

	// Get params
	$color = $this->params->get( 'templatecolor' );
	$logo = $this->params->get( 'logo' );

	$templateparams = $app->getTemplate( true )->params;


	if ( $color == 'image' ) {
		$this->addStyleDeclaration( "
	.logoheader {
		background: url('" . $this->baseurl . "/" . htmlspecialchars( $headerImage ) . "') no-repeat right;
	}
	body {
		background: " . $templateparams->get( 'backgroundcolor' ) . ";
	}" );
	}

	JHtml::_( 'bootstrap.framework' );


?>


<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>">
<head>
	<?php echo $template->getHead( $doc, $config, $app );
		$styles = array(
			'basic/mdb.css',
			'bootstrap/bootstrap.css',
			'bootstrap/bootstrap-dialog.css',
			'fonts/font-awesome.css',
			'fonts/icomoon.css',
			'slick/slick.css',
			'style.css'
		);
		echo $template->getCss( $styles );
	?>


	<script src="templates/tiemenrtuinstra/assets/js/jquery-2.2.3.min.js"></script>


</head>

<body>
<jdoc:include type="modules" name="mainmenu"/>

<!--/.Navbar-->
<jdoc:include type="modules" name="content-top"/>
<jdoc:include type="message"/>
<jdoc:include type="component"/>
<jdoc:include type="modules" name="content-bottom"/>

<?php

	if ( $menu->getActive() == $menu->getDefault( $lang->getTag() ) ) {
		include_once '/html/includes/whatilikeanduse.php';
	}
?>
<jdoc:include type="modules" name="breadcrumbs"/>
<!--Footer-->
<footer class="page-footer center-on-small-only">
	<!--Call to action-->
	<div class="call-to-action">
		<h4>Volg mij op social media</h4>
		<ul>
			<li>
				<a target="_blank" href="https://www.facebook.com/tiemen.tuinstra" class="btn-floating btn-small btn-fb"><i class="fa fa-facebook"></i></a>
			</li>
			<li>
				<a target="_blank" href="https://plus.google.com/u/0/+TiemenTuinstra/posts" class="btn-floating btn-small btn-gplus"><i class="fa fa-google-plus"></i></a>
			</li>
			<li>
				<a target="_blank" href="https://twitter.com/tiementuinstra" class="btn-floating btn-small btn-tw"><i class="fa fa-twitter"></i></a>
			</li>
			<li>
				<a target="_blank" href="https://www.youtube.com/channel/UC0w7EIoB6IyT_EltK81EGzA" class="btn-floating btn-small btn-yt"><i class="fa fa-youtube"></i></a>
			</li>
			<li>
				<a target="_blank" href="https://www.instagram.com/tiementuinstra/" class="btn-floating btn-small btn-ins"><i class="fa fa-instagram"></i></a>
			</li>
			<li>
				<a href="mailto:mail@tiemenrtuinstra.nl" class="btn-floating btn-small btn-email"><i class="fa fa-envelope"></i></a>
			<li>
				<a class="btn-floating btn-small btn-social btn-wh" href="whatsapp://send?abid=username&text=Hello%2C%20World!"><i class="fa fa-whatsapp"></i></a>
			</li>

		</ul>
	</div>
	<!--/.Call to action-->
	<jdoc:include type="modules" name="footermenu"/>
	<!--Copyright-->
	<div class="footer-copyright">
		<div class="container-fluid">
			© 1989-<?php echo date( 'Y' ); ?> Copyright <a href="http://tiemenrtuinstra.nl">TiemenRTuinstra.nl</a>
			| <?php echo date( 'Y' ) - 1989; ?> jaar aan levenservaring | 's-Gravenhage - Baarn
		</div>
	</div>
	<!--/.Copyright-->

</footer>
<!--/.Footer-->


<script src="templates/tiemenrtuinstra/assets/js/tether.min.js"></script>
<script src="templates/tiemenrtuinstra/assets/js/bootstrap.js"></script>
<script src="templates/tiemenrtuinstra/assets/js/bootstrap-dialog.js"></script>
<script src="templates/tiemenrtuinstra/assets/js/mdb.js"></script>
<script src="templates/tiemenrtuinstra/assets/js/slick.js"></script>
<script src="templates/tiemenrtuinstra/assets/js/custom.js"></script>


</body>

</html>