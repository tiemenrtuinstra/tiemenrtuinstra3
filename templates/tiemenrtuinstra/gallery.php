<?php include_once('header.php'); ?>
	<!--Featured Image-->
	<div id="content-head" class="view overlay hm-white-slight z-depth-2"
		 style="background-image: url(assets/img/background/background2.jpg);">
		<div class="full-bg-img flex-center">
			<ul class="animated fadeIn col-md-12">
				<li>
					<img src="template/tiemenrtuinstra/assets/img/logo/logo1000x1000.png" class="center-block" height="212px">
					<h1 class="h1-responsive flex-item">TiemenRTuinstra.nl</h1>
				</li>
			</ul>
			<a href="#!">
				<div class="mask waves-effect waves-light"></div>
			</a>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<!--Post data-->
				<div class="jumbotron m-1 text-xs-center">
					<h1 class="h1-responsive">Post title</h1>
					<hr>

				</div>
				<!--/.Post data-->
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div id="gallery-content" class="view overlay hm-white-slight z-depth-2">
					<div class="z-depth-2" style="display: inline-block;margin-right:-1px;">
					<div id="mdb-lightbox-ui"><!-- Root element of PhotoSwipe. Must have class pswp. -->
						<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

							<!-- Background of PhotoSwipe.
										 It's a separate element, as animating opacity is faster than rgba(). -->
							<div class="pswp__bg"></div>

							<!-- Slides wrapper with overflow:hidden. -->
							<div class="pswp__scroll-wrap">

								<!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
								<!-- don't modify these 3 pswp__item elements, data is added later on. -->
								<div class="pswp__container" style="transform: translate3d(0px, 0px, 0px);">
									<div class="pswp__item"
										 style="display: block; transform: translate3d(-2150px, 0px, 0px);">
										<div class="pswp__zoom-wrap"
											 style="transform: translate3d(216px, 44px, 0px) scale(1);"><img
												class="pswp__img"
												src="http://mdbootstrap.com/images/regular/nature/img%20(5).jpg"
												style="opacity: 1; width: 1488px; height: 992px;"></div>
									</div>
									<div class="pswp__item" style="transform: translate3d(0px, 0px, 0px);">
										<div class="pswp__zoom-wrap"
											 style="transform: translate3d(216px, 44px, 0px) scale(1);"><img
												class="pswp__img pswp__img--placeholder"
												src="http://mdbootstrap.com/images/regular/nature/img%20(6).jpg"
												style="width: 1315px; height: 877px; display: none;"><img
												class="pswp__img"
												src="http://mdbootstrap.com/images/regular/nature/img%20(6).jpg"
												style="display: block; width: 1488px; height: 992px;"></div>
									</div>
									<div class="pswp__item"
										 style="display: block; transform: translate3d(2150px, 0px, 0px);">
										<div class="pswp__zoom-wrap"
											 style="transform: translate3d(216px, 44px, 0px) scale(1);"><img
												class="pswp__img"
												src="http://mdbootstrap.com/images/regular/nature/img%20(7).jpg"
												style="opacity: 1; width: 1488px; height: 992px;"></div>
									</div>
								</div>

								<!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
								<div class="pswp__ui pswp__ui--fit pswp__ui--hidden">

									<div class="pswp__top-bar">

										<!--  Controls are self-explanatory. Order can be changed. -->

										<div class="pswp__counter">5 / 9</div>

										<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

										<!--<button class="pswp__button pswp__button--share" title="Share"></button>-->

										<button class="pswp__button pswp__button--fs"
												title="Toggle fullscreen"></button>

										<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

										<!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
										<!-- element will get class pswp__preloader--active when preloader is running -->
										<div class="pswp__preloader">
											<div class="pswp__preloader__icn">
												<div class="pswp__preloader__cut">
													<div class="pswp__preloader__donut"></div>
												</div>
											</div>
										</div>
									</div>

									<!--
											<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
												<div class="pswp__share-tooltip"></div>
											</div>
											   -->

									<button class="pswp__button pswp__button--arrow--left"
											title="Previous (arrow left)">
									</button>

									<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
									</button>

									<div class="pswp__caption">
										<div class="pswp__caption__center"></div>
									</div>

								</div>

							</div>

						</div>
					</div>

					<div class="mdb-lightbox no-margin" data-pswp-uid="1">


						<figure class="col-md-4">
							<a href="http://mdbootstrap.com/images/regular/nature/img%20(4).jpg" data-size="1600x1067">
								<img src="http://mdbootstrap.com/images/regular/nature/img%20(4).jpg" class="img-fluid">
							</a>
						</figure>

						<figure class="col-md-4">
							<a href="http://mdbootstrap.com/images/regular/nature/img%20(2).jpg" data-size="1600x1067">
								<img src="http://mdbootstrap.com/images/regular/nature/img%20(2).jpg" class="img-fluid">
							</a>
						</figure>

						<figure class="col-md-4">
							<a href="http://mdbootstrap.com/images/regular/nature/img%20(3).jpg" data-size="1600x1067">
								<img src="http://mdbootstrap.com/images/regular/nature/img%20(3).jpg" class="img-fluid">
							</a>
						</figure>

						<figure class="col-md-4">
							<a href="http://mdbootstrap.com/images/regular/nature/img%20(5).jpg" data-size="1600x1067">
								<img src="http://mdbootstrap.com/images/regular/nature/img%20(5).jpg" class="img-fluid">
							</a>
						</figure>

						<figure class="col-md-4">
							<a href="http://mdbootstrap.com/images/regular/nature/img%20(6).jpg" data-size="1600x1067">
								<img src="http://mdbootstrap.com/images/regular/nature/img%20(6).jpg" class="img-fluid">
							</a>
						</figure>

						<figure class="col-md-4">
							<a href="http://mdbootstrap.com/images/regular/nature/img%20(7).jpg" data-size="1600x1067">
								<img src="http://mdbootstrap.com/images/regular/nature/img%20(7).jpg" class="img-fluid">
							</a>
						</figure>

						<figure class="col-md-4">
							<a href="http://mdbootstrap.com/images/regular/nature/img%20(8).jpg" data-size="1600x1067">
								<img src="http://mdbootstrap.com/images/regular/nature/img%20(8).jpg" class="img-fluid">
							</a>
						</figure>

						<figure class="col-md-4">
							<a href="http://mdbootstrap.com/images/regular/nature/img%20(9).jpg" data-size="1600x1067">
								<img src="http://mdbootstrap.com/images/regular/nature/img%20(9).jpg" class="img-fluid">
							</a>
						</figure>

						<figure class="col-md-4">
							<a href="http://mdbootstrap.com/images/regular/nature/img%20(11).jpg" data-size="1600x1067">
								<img src="http://mdbootstrap.com/images/regular/nature/img%20(11).jpg"
									 class="img-fluid">
							</a>
						</figure>

					</div>
				</div>


				</div>


			</div>
		</div>
	</div>



	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div id="gallery-paginiation-content" class="view overlay hm-white-slight z-depth-2">

	<ul class="pagination pull-left">
		<li class="page-item">
			<a href="" class="page-link" style="padding-top:10px;"><i class="fa fa-chevron-left"></i></a>
		</li>
		</ul>
					<ul class="pagination pull-right">
		<li class="page-item disabled">
			<a class="page-link" href="#" aria-label="Previous">
				<span aria-hidden="true">&laquo;</span>
				<span class="sr-only">Previous</span>
			</a>
		</li>
		<li class="page-item active">
			<a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
		</li>
		<li class="page-item"><a class="page-link" href="#">2</a></li>
		<li class="page-item"><a class="page-link" href="#">3</a></li>
		<li class="page-item"><a class="page-link" href="#">4</a></li>
		<li class="page-item"><a class="page-link" href="#">5</a></li>
		<li class="page-item">
			<a class="page-link" href="#" aria-label="Next">
				<span aria-hidden="true">&raquo;</span>
				<span class="sr-only">Next</span>
			</a>
		</li>
	</ul>

					</div></div></div></div>
<?php include_once('footer.php'); ?>