<?php include_once ('header.php'); ?>
	<!--Featured Image-->
	<div id="content-head" class="view overlay hm-white-slight z-depth-2" style="background-image: url(assets/img/background/background2.jpg);">
		<div class="full-bg-img flex-center">
			<ul class="animated fadeIn col-md-12">
				<li>
					<img src="template/tiemenrtuinstra/assets/img/logo/logo1000x1000.png" class="center-block" height="212px">
					<h1 class="h1-responsive flex-item">TiemenRTuinstra.nl</h1>
				</li>
			</ul>
			<a href="#!">
				<div class="mask waves-effect waves-light"></div>
			</a>
		</div>
	</div>
	<div class="container"><div class="row"><div class="col-xs-12">
				<!--Post data-->
				<div class="jumbotron m-1 text-xs-center">
					<h1 class="h1-responsive">Post title</h1>
					<hr>
					<div class="text-justify">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id volutpat nibh. Duis rutrum non lectus at fringilla. Fusce tincidunt rutrum lacus sed iaculis. Proin hendrerit arcu sed justo tempus gravida. Donec pharetra nibh sit amet lacus porttitor, ut tempor ante tincidunt. Pellentesque a pretium nulla, eleifend volutpat est. Integer ultricies diam ipsum, vel fermentum sem vestibulum id.
					</div>
				</div>
				<!--/.Post data-->
			</div></div></div>

	<div class="container"><div class="row"><div class="col-xs-12">
	<!--Main wrapper-->
<div id="portfolio-content" class="jumbotron horizontal-listing z-depth-1 ">
	<!--First row-->
	<div class="row">
		<!--Image column-->
		<div class="col-sm-4">
			<div class="view overlay hm-white-slight">
				<img src="http://mdbootstrap.com/images/reg/reg%20(54).jpg" class="img-fluid" alt="">
				<a>
					<div class="mask"></div>
				</a>
			</div>
		</div>
		<!--/.Image column-->

		<!--Content column-->
		<div class="col-sm-8">
			<a><h2>Card title</h2></a>

			<div class="card-data">
				<ul>
					<li><i class="fa fa-clock-o"></i> 05/10/2015</li>
					<li><a><i class="fa fa-comments-o"></i>12</a></li>
					<li><a><i class="fa fa-facebook"> </i>21</a></li>
					<li><a><i class="fa fa-twitter"> </i>5</a></li>
				</ul>
			</div>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis incidunt accusantium maxime odit nemo corporis, magnam quam eos quasi architecto inventore provident hic neque aspernatur, ipsa tempore vero numquam totam.</p>
		</div>
		<!--/.Content column-->

	</div>
	<!--/.First row-->

	<!--Second row-->
	<div class="row">
		<!--Image column-->
		<div class="col-sm-4">
			<div class="view overlay hm-white-slight">
				<img src="http://mdbootstrap.com/images/reg/reg%20(55).jpg" class="img-fluid" alt="">
				<a>
					<div class="mask"></div>
				</a>
			</div>
		</div>
		<!--/.Image column-->

		<!--Content column-->
		<div class="col-sm-8">
			<a><h2>Card title</h2></a>

			<div class="card-data">
				<ul>
					<li><i class="fa fa-clock-o"></i> 05/10/2015</li>
					<li><a><i class="fa fa-comments-o"></i>12</a></li>
					<li><a><i class="fa fa-facebook"> </i>21</a></li>
					<li><a><i class="fa fa-twitter"> </i>5</a></li>
				</ul>
			</div>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis incidunt accusantium maxime odit nemo corporis, magnam quam eos quasi architecto inventore provident hic neque aspernatur, ipsa tempore vero numquam totam.</p>
		</div>
		<!--/.Content column-->

	</div>
	<!--/.Second row-->

	<!--Third row-->
	<div class="row">
		<!--Image column-->
		<div class="col-sm-4">
			<div class="view overlay hm-white-slight">
				<img src="http://mdbootstrap.com/images/reg/reg%20(56).jpg" class="img-fluid" alt="">
				<a>
					<div class="mask"></div>
				</a>
			</div>
		</div>
		<!--/.Image column-->

		<!--Content column-->
		<div class="col-sm-8">
			<a><h2>Card title</h2></a>

			<div class="card-data">
				<ul>
					<li><i class="fa fa-clock-o"></i> 05/10/2015</li>
					<li><a><i class="fa fa-comments-o"></i>12</a></li>
					<li><a><i class="fa fa-facebook"> </i>21</a></li>
					<li><a><i class="fa fa-twitter"> </i>5</a></li>
				</ul>
			</div>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis incidunt accusantium maxime odit nemo corporis, magnam quam eos quasi architecto inventore provident hic neque aspernatur, ipsa tempore vero numquam totam.</p>
		</div>
		<!--/.Content column-->

	</div>
	<!--/.Third row-->

</div>
<!--/.Main wrapper-->
</div></div></div>
<?php include_once ('footer.php'); ?>