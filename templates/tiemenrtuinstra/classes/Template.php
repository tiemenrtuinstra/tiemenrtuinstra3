<?php
	/**
	 * Created by PhpStorm.
	 * User: TiemenReinder
	 * Date: 11-9-2016
	 * Time: 13:34
	 */

	use Joomla\Registry\Registry;


	class Template
	{
		protected $template_url = 'templates/tiemenrtuinstra';

		/**
		 * @param JDocumentHtml   $document
		 * @param Registry        $config
		 * @param JApplicationCms $app
		 *
		 * @return string
		 */
		public function getHead( JDocument $document, Joomla\Registry\Registry $config, JApplicationCms $app){
			$this->template_url;
			$head = $this->getTitle( $document, $config);
			$head .= $this->getMetaInformation( $document);
			$head .= $this->getIcons();

			return $head;
		}

		/**
		 * @param JDocumentHtml $document
		 * @param Registry      $config
		 *
		 * @return string
		 */
		private function getTitle( JDocument $document, Joomla\Registry\Registry $config){
			return '<title>'.$document->getTitle().' | '.$config->get( 'sitename' ).'</title>';
		}

		/**
		 * @param JDocumentHtml $document
		 *
		 * @return string
		 */
		private function getMetaInformation( JDocument $document){
			return '
			<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=3.0, user-scalable=yes"/>
			<meta name="HandheldFriendly" content="true"/>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<meta http-equiv="x-ua-compatible" content="ie=edge">
			<meta name="description" content="'.$document->getMetaData( 'description' ).'">
			<meta name="keywords" content="'.$document->getMetaData( 'keywords' ).'">
			<meta name="author" content="'.$document->getMetaData( 'author' ).'">
			<meta name="theme-color" content="rgba(12,62,1,1)">';
		}

		private function getIcons(){
			return '<link rel="shortcut icon" href="'.$this->template_url.'/assets/img/logo/logo-navbar.png">
			<link rel="icon" sizes="192x192" href="'.$this->template_url.'/assets/img/logo/logo-theme.png">';
		}

		public function getCss(array $styles){
			$css = '';
			foreach ($styles as $style){
				$css .= '<link rel="stylesheet" type="text/css" href="'.$this->template_url.'/assets/css/'.$style.'">';
			}
			return $css;
		}
	}