<?php include_once( 'header.php' ); ?>


	<div id="carousel-example-1" class="carousel slide carousel-fade z-depth-2" data-ride="carousel"
	     data-interval="false">
		<!--Indicators-->
		<ol class="carousel-indicators">
			<li data-target="#carousel-example-1" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-1" data-slide-to="1"></li>
			<li data-target="#carousel-example-1" data-slide-to="2"></li>
		</ol>

		<!--/.Indicators-->

		<!--Slides-->
		<div class="carousel-inner" role="listbox">

			<!--First slide-->
			<div class="carousel-item active">
				<!--Mask-->
				<div class="view overlay hm-white-slight">
					<div class="full-bg-img flex-center">
						<ul class="animated fadeIn col-md-12">
							<li>
								<img src="template/tiemenrtuinstra/assets/img/logo/logo1000x1000.png" class="center-block" height="212px">
								<h1 class="h1-responsive flex-item">TiemenRTuinstra.nl</h1>
							</li>
							<li>
								<p class="flex-item">The most powerful and free UI KIT for Bootstrap</p>
							</li>
							<li>
								<a target="_blank" href="http://mdbootstrap.com/material-design-for-bootstrap/"
								   class="btn btn-default btn-lg flex-item waves-effect waves-light">Learn more</a>
							</li>
						</ul>
					</div>
				</div>
				<!--/.Mask-->
			</div>
			<!--/.First slide-->

			<!--Second slide -->
			<div class="carousel-item">
				<!--Mask-->
				<div class="view hm-black-light">
					<div class="full-bg-img flex-center">
						<ul class="animated fadeIn col-md-12">
							<li>
								<img src="template/tiemenrtuinstra/assets/img/logo/logo1000x1000.png" class="center-block" height="212px">
								<h1 class="h1-responsive">TiemenRTuinstra.nl</h1>
							</li>
							<li>
								<p>And all of them are FREE!</p>
							</li>
							<li>
								<a target="_blank" href="http://mdbootstrap.com/bootstrap-tutorial/"
								   class="btn btn-primary btn-lg waves-effect waves-light">Start learning</a>
							</li>
						</ul>
					</div>
				</div>
				<!--/.Mask-->
			</div>
			<!--/.Second slide -->

			<!--Third slide-->
			<div class="carousel-item">
				<!--Mask-->
				<div class="view hm-black-light">
					<div class="full-bg-img flex-center">
						<ul class="animated fadeIn col-md-12">
							<li>
								<img src="template/tiemenrtuinstra/assets/img/logo/logo1000x1000.png" class="center-block" height="212px">
								<h1 class="h1-responsive">TiemenRTuinstra.nl</li>
							<li>
								<p>Our community can help you with any question</p>
							</li>
							<li>
								<a target="_blank" href="http://mdbootstrap.com/forums/forum/support/"
								   class="btn btn-default btn-lg waves-effect waves-light">Support forum</a>
							</li>
						</ul>
					</div>
				</div>
				<!--/.Mask-->
			</div>
			<!--/.Third slide-->

		</div>
		<!--/.Slides-->

		<!--Controls-->
		<a class="left carousel-control" href="#carousel-example-1" role="button" data-slide="prev">
			<span class="icon-prev" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-example-1" role="button" data-slide="next">
			<span class="icon-next" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
		<!--/.Controls-->
	</div>


	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<!--Post data-->
				<div class="jumbotron m-1 text-xs-center">
					<h1 class="h1-responsive">Post title</h1>
					<hr>
					<div class="text-justify">
						Welkom op mijn website, het is een website over mijzelf, maar vooral ook een plek voor mij om de
						nieuwste technieken op het gebied van webdeveloping en webdesign uit te proberen.
					</div>
				</div>
				<!--/.Post data-->
			</div>
		</div>
	</div>


	<!--Content-->
	<div id="content" class="container">
		<div class="row">
			<!--First columnn-->
			<div class="col-xs-12 col-md-4">
				<!--Card-->
				<div class="card">

					<!--Card image-->
					<div class="view overlay hm-white-slight">
						<img src="assets/img/background/background1.jpg" class="img-fluid" alt="">
						<a href="#!">
							<div class="mask waves-effect waves-light"></div>
						</a>
					</div>
					<!--/.Card image-->

					<!--Card content-->
					<div class="card-block">
						<!--Title-->
						<h4 class="card-title">Card title</h4>
						<!--Text-->
						<p class="card-text">Some quick example text to build on the card title and make up the bulk of
							the card's content.</p>
						<a href="#" class="btn btn-default">Read more</a>
					</div>
					<!--/.Card content-->

				</div>
				<!--/.Card-->
			</div>
			<!--First columnn-->

			<!--Second columnn-->
			<div class="col-xs-12 col-md-4">
				<!--Card-->
				<div class="card">

					<!--Card image-->
					<div class="view overlay hm-white-slight">
						<img src="assets/img/background/background2.jpg" class="img-fluid" alt="">
						<a href="#!">
							<div class="mask waves-effect waves-light"></div>
						</a>
					</div>
					<!--/.Card image-->

					<!--Card content-->
					<div class="card-block">
						<!--Title-->
						<h4 class="card-title">Card title</h4>
						<!--Text-->
						<p class="card-text">Some quick example text to build on the card title and make up the bulk of
							the card's content.</p>
						<a href="#" class="btn btn-default">Read more</a>
					</div>
					<!--/.Card content-->

				</div>
				<!--/.Card-->
			</div>
			<!--Second columnn-->

			<!--Third columnn-->
			<div class="col-xs-12 col-md-4">
				<!--Card-->
				<div class="card">

					<!--Card image-->
					<div class="view overlay hm-white-slight">
						<img src="assets/img/background/background3.jpg" class="img-fluid" alt="">
						<a href="#!">
							<div class="mask waves-effect waves-light"></div>
						</a>
					</div>
					<!--/.Card image-->

					<!--Card content-->
					<div class="card-block">
						<!--Title-->
						<h4 class="card-title">Card title</h4>
						<!--Text-->
						<p class="card-text">Some quick example text to build on the card title and make up the bulk of
							the card's content.</p>
						<a href="#" class="btn btn-default">Read more</a>
					</div>
					<!--/.Card content-->

				</div>
				<!--/.Card-->
			</div>
			<!--Third columnn-->
		</div>
	</div>
	<!--/.Content-->
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<!--Call to action-->
				<div class="landing call-to-action">
					<h2 class="h2-responsive text-xs-center" style="margin-top: 30px;">What I like and use...</h2>
					<hr>
					<ul class="center-block" style="    padding: 0;
    height: 60px;
    display: block;
    margin-bottom: 60px;
        margin-top: -75px;">
						<li class="col-xs-3"><a target="_blank" href="https://www.facebook.com/tiemen.tuinstra"
						                        class="btn-call-to-action btn-android"><i class="icon-android"></i></a>
						</li>
						<li class="col-xs-3"><a target="_blank" href="https://www.facebook.com/tiemen.tuinstra"
						                        class="btn-call-to-action btn-bootstrap"><i class="icon-bootstrap"></i></a>
						</li>
						<li class="col-xs-3"><a target="_blank" href="https://www.facebook.com/tiemen.tuinstra"
						                        class="btn-call-to-action btn-microsoft"><i class="icon-microsoft"></i></a>
						</li>
						<li class="col-xs-3"><a target="_blank" href="https://www.facebook.com/tiemen.tuinstra"
						                        class="btn-call-to-action btn-office"><i class="icon-office"></i></a>
						</li>
					</ul>
					<ul class="center-block">

						<li class="col-xs-1"><a target="_blank" href="https://www.facebook.com/tiemen.tuinstra"
						                        class="btn-call-to-action btn-adobe"><i class="icon-adobe"></i></a></li>
						<li class="col-xs-1"><a target="_blank" href="https://www.facebook.com/tiemen.tuinstra"
						                        class="btn-call-to-action"><i class="icon-wosm"></i></a></li>
						<li class="col-xs-1"><a target="_blank" href="https://www.facebook.com/tiemen.tuinstra"
						                        class="btn-call-to-action"><i class="icon-xbox"></i></a></li>
						<li class="col-xs-1"><a target="_blank" href="https://www.facebook.com/tiemen.tuinstra"
						                        class="btn-call-to-action"><i class="fa fa-html5"></i></a></li>
						<li class="col-xs-1"><a target="_blank" href="https://www.facebook.com/tiemen.tuinstra"
						                        class="btn-call-to-action"><i class="fa fa-css3"></i></a></li>
						<li class="col-xs-1"><a target="_blank" href="https://www.facebook.com/tiemen.tuinstra"
						                        class="btn-call-to-action"><i class="fa fa-joomla"></i></a></li>
						<li class="col-xs-1"><a target="_blank" href="https://www.facebook.com/tiemen.tuinstra"
						                        class="btn-call-to-action"><i class="fa fa-bitbucket"></i></a></li>
						<li class="col-xs-1"><a target="_blank" href="https://www.facebook.com/tiemen.tuinstra"
						                        class="btn-call-to-action"><i class="icon-javascript"></i></a></li>
						<li class="col-xs-1"><a target="_blank" href="https://www.facebook.com/tiemen.tuinstra"
						                        class="btn-call-to-action"><i class="icon-php"></i></a></li>


					</ul>
				</div>

			</div>
		</div>
	</div>


<?php include_once( 'footer.php' ); ?>