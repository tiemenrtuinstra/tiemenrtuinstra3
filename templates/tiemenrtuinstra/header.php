<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>TiemenRTuinstra.nl</title>

	<!-- Font Awesome -->
	<link rel="stylesheet" href="assets/css/fonts/font-awesome.css">
	<link rel="stylesheet" href="assets/css/fonts/icomoon.css">

	<!-- Bootstrap core CSS -->
	<link href="assets/css/bootstrap/bootstrap.css" rel="stylesheet">

	<!-- Material Design Bootstrap -->
	<link href="assets/css/basic/mdb.css" rel="stylesheet">

	<!-- Template styles -->
	<link href="assets/css/style.css" rel="stylesheet">

	<link rel="shourtcut icon" href="assets/img/logo/logo-navbar.png">
	<meta name="theme-color" content="rgba(12,62,1,1)">

	<link rel="icon" sizes="192x192" href="assets/img/logo/logo-theme.png">
</head>

<body>


<!--Navbar-->
<nav class="navbar navbar-dark navbar-fixed-top scrolling-navbar">
	<!--Navbar Brand-->
	<a class="navbar-brand hidden-lg-up" href="http://tiemenrtuinstra.nl" ><img src="assets/img/logo/logo-navbar.png"> TiemenRTuinstra.nl</a>
	<!-- Collapse button-->
	<!-- SideNav slide-out button -->

	<a href="#" data-activates="slide-out" class="button-collapse waves-effect waves-light"><i class="fa fa-bars"></i></a>
	<!--/. SideNav slide-out button -->

	<div class="container">

		<!--Collapse content-->
		<div class="hidden-md-down">

			<!--Links-->
			<ul class="nav navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="landing.php">Landing <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="content.php">Content</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="blog.php">blog</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="gallery.php">Gallery</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="contact.php">Contact</a>
				</li>
			</ul>
		</div>
		<!--/.Collapse content-->

	</div>

</nav>
<!--/.Navbar-->