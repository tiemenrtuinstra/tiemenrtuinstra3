
<!--Footer-->
<footer class="page-footer center-on-small-only">


	<!--Call to action-->
	<div class="call-to-action">
		<h4>Volg mij op social media</h4>
		<ul>
			<li><a target="_blank" href="https://www.facebook.com/tiemen.tuinstra" class="btn-floating btn-small btn-fb"><i class="fa fa-facebook"></i></a></li>
			<li><a target="_blank" href="https://plus.google.com/u/0/+TiemenTuinstra/posts" class="btn-floating btn-small btn-gplus"><i class="fa fa-google-plus"></i></a></li>
			<li><a target="_blank" href="https://twitter.com/tiementuinstra" class="btn-floating btn-small btn-tw"><i class="fa fa-twitter"></i></a></li>
			<li><a target="_blank" href="https://www.youtube.com/channel/UC0w7EIoB6IyT_EltK81EGzA" class="btn-floating btn-small btn-yt"><i class="fa fa-youtube"></i></a></li>
			<li><a target="_blank" href="https://www.instagram.com/tiementuinstra/" class="btn-floating btn-small btn-ins"><i class="fa fa-instagram"></i></a></li>
			<li><a href="mailto:mail@tiemenrtuinstra.nl" class="btn-floating btn-small btn-email"><i class="fa fa-envelope"></i></a>
			<li><a class="btn-floating btn-small btn-social btn-wh" href="whatsapp://send?abid=username&text=Hello%2C%20World!"><i class="fa fa-whatsapp"></i></a></li>

		</ul>
	</div>
	<!--/.Call to action-->

	<!--Copyright-->
	<div class="footer-copyright">
		<div class="container-fluid">
			© <?php echo date('Y'); ?> Copyright <a href="http://tiemenrtuinstra.nl">TiemenRTuinstra.nl</a>
		</div>
	</div>
	<!--/.Copyright-->

</footer>
<!--/.Footer-->

<!-- Sidebar navigation -->
<ul id="slide-out" class="side-nav fixed default-side-nav light-side-nav">

	<!-- Logo -->
	<div class="logo-wrapper waves-light">
		<a class="navbar-brand" href="http://tiemenrtuinstra.nl"><img src="assets/img/logo/logo-navbar.png"> TiemenRTuinstra.nl</a>
	</div>

	<!--/. Logo -->

	<!-- Side navigation links -->
	<ul class="collapsible collapsible-accordion">
		<li><a class="collapsible-header waves-effect">Click me</a>
			<div class="collapsible-body">
				<ul>
					<li><a href="#" class="waves-effect">Link</a>
					</li>
					<li><a href="#" class="waves-effect">Link</a>
					</li>
				</ul>
			</div>
		</li>
		<li><a class="collapsible-header waves-effect">Click me</a>
			<div class="collapsible-body">
				<ul>
					<li><a href="#" class="waves-effect">Link</a>
					</li>
					<li><a href="#" class="waves-effect">Link</a>
					</li>
				</ul>
			</div>
		</li>
		<li><a class="collapsible-header waves-effect">Click me</a>
			<div class="collapsible-body">
				<ul>
					<li><a href="#" class="waves-effect">Link</a>
					</li>
					<li><a href="#" class="waves-effect">Link</a>
					</li>
				</ul>
			</div>
		</li>
	</ul>
	<!--/. Side navigation links -->
</ul>

<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="assets/js/jquery-2.2.3.min.js"></script>

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="assets/js/tether.min.js"></script>

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="assets/js/mdb.min.js"></script>

<script>
	$(".button-collapse").sideNav();


	$(document).ready(function () {
		$('#carousel-example-1').hammer().on('swipeleft', function () {
			$(this).carousel('next');
		})
		$('#carousel-example-1').hammer().on('swiperight', function () {
			$(this).carousel('prev');
		})
	});
</script>
</body>

</html>